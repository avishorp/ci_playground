#include <iostream>

#define _STR(s) _XSTR(s)
#define _XSTR(s) #s

int main() {
  std::cout << "This is hello world, version " << _STR(VERSION) << " on branch " << _STR(BRANCH) << std::endl;
  std::cout << "Hello world, from master branch" << std::endl;
}
